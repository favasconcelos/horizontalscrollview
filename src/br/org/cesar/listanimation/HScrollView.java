package br.org.cesar.listanimation;

import android.animation.IntEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.BounceInterpolator;
import android.view.animation.LinearInterpolator;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;

public class HScrollView extends HorizontalScrollView {

	private int mInitWidth;
	private int mMaxWidth;

	private boolean mIsTouching;
	private int mLastDeltaX;

	public HScrollView(Context context) {
		super(context);
		mIsTouching = false;
	}

	public void setInitWidth(int size) {
		mInitWidth = size;
	}

	public void setMaxWidth(int size) {
		mMaxWidth = size;
	}

	@Override
	protected boolean overScrollBy(int deltaX, int deltaY, int scrollX, int scrollY, int scrollRangeX, int scrollRangeY, int maxOverScrollX, int maxOverScrollY, boolean isTouchEvent) {
		final boolean clamped = super.overScrollBy(deltaX, deltaY, scrollX, scrollY, scrollRangeX, scrollRangeY, 1, maxOverScrollY, isTouchEvent);
		if (clamped && isTouchEvent) {
			int value = Math.abs(deltaX);
			mIsTouching = true;
			if (mLastDeltaX < value) {
				mLastDeltaX = value;
				changeViewSize(mLastDeltaX);
			}
		}
		return clamped;
	}

	@Override
	public boolean onTouchEvent(MotionEvent ev) {
		int action = ev.getAction();
		if (action == MotionEvent.ACTION_CANCEL || action == MotionEvent.ACTION_UP) {
			if (mIsTouching) {
				final int value = mLastDeltaX;
				mLastDeltaX = 0;
				mIsTouching = false;
				animateBack(value);
			}
		}
		return super.onTouchEvent(ev);
	}

	private void animateBack(int deltaX) {
		ViewGroup root = (ViewGroup) getChildAt(0);
		int i = 0;
		while (i < root.getChildCount()) {
			ViewGroup container = (ViewGroup) root.getChildAt(i);
			ImageView image = (ImageView) container.getChildAt(0);

			ValueAnimator anim = ObjectAnimator.ofObject(new ViewEvaluator(image), mInitWidth + deltaX, mInitWidth);
			anim.setDuration(200);
			anim.setInterpolator(new LinearInterpolator());
			anim.start();

			++i;
		}
	}

	private void changeViewSize(int deltaX) {
		ViewGroup root = (ViewGroup) getChildAt(0);
		int i = 0;
		while (i < root.getChildCount()) {
			ViewGroup container = (ViewGroup) root.getChildAt(i);
			ImageView image = (ImageView) container.getChildAt(0);

			int value = Math.min(mInitWidth + deltaX, mMaxWidth);

			ViewGroup.LayoutParams params = image.getLayoutParams();
			params.width = value;
			image.setLayoutParams(params);
			++i;
		}
	}

	class ViewEvaluator extends IntEvaluator {
		private View mView;

		public ViewEvaluator(View view) {
			mView = view;
		}

		@Override
		public Integer evaluate(float fraction, Integer startValue, Integer endValue) {
			int n = super.evaluate(fraction, startValue, endValue);
			ViewGroup.LayoutParams params = mView.getLayoutParams();
			params.width = n;
			mView.setLayoutParams(params);
			return n;
		}
	}

}
