package br.org.cesar.listanimation;

import android.app.Activity;
import android.os.Bundle;
import android.view.Gravity;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;

public class MainActivity extends Activity {

	private static final int INIT_WIDTH = 100;
	private static final int MAX_WIDTH = 150;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		HorizontalScrollView.LayoutParams rParams = new HorizontalScrollView.LayoutParams(HorizontalScrollView.LayoutParams.MATCH_PARENT, HorizontalScrollView.LayoutParams.MATCH_PARENT);
		LinearLayout rootLayout = new LinearLayout(this);
		rootLayout.setLayoutParams(rParams);

		for (int i = 0; i < 20; i++) {
			LinearLayout.LayoutParams cParams = new LinearLayout.LayoutParams(MAX_WIDTH, 200);
			cParams.rightMargin = 20;

			FrameLayout.LayoutParams iParams = new FrameLayout.LayoutParams(INIT_WIDTH, FrameLayout.LayoutParams.WRAP_CONTENT);
			iParams.gravity = Gravity.CENTER_HORIZONTAL;

			FrameLayout container = new FrameLayout(this);
			container.setLayoutParams(cParams);
			// container.setBackgroundColor(Color.BLACK);

			ImageView image = new ImageView(this);
			image.setLayoutParams(iParams);

			// if (i % 2 == 0) {
			// image.setBackgroundColor(Color.RED);
			// } else {
			// image.setBackgroundColor(Color.BLUE);
			// }

			image.setImageResource(R.drawable.ic_launcher);

			container.addView(image);
			rootLayout.addView(container);
		}

		HScrollView hScroll = new HScrollView(this);
		hScroll.setInitWidth(INIT_WIDTH);
		hScroll.setMaxWidth(MAX_WIDTH);
		hScroll.addView(rootLayout);
		setContentView(hScroll);
	}
}
